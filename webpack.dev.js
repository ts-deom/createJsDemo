const path = require('path');
const merge = require('webpack-merge');
// 引入通用webpack配置文件
const common = require('./webpack.common.js');

module.exports = merge(common, {
  module: {
    rules:
      [
        {   //ts-loader只解析.ts .tsx扩展类型
          test: /\.tsx?$/,          
          loader: 'ts-loader',
          exclude: /node_modules/
        },
      ]
  },
  resolve:{
    extensions: ['.ts','.js'],
  },
  // 使用 source-map
  devtool: 'source-map',
  // 对 webpack-dev-server 进行配置
  devServer: {
    contentBase: './dist',
    //配置可以通过本级ip地址访问
    host: '0.0.0.0',
    // 自动打开浏览器
    open: true,
  },
  plugins: [
  ],
  // 设置出口文件地址与文件名
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.min.js'
  },
});
