const path = require('path');
module.exports = {
  entry: ['babel-polyfill', './src/main.ts'],//引入babel-polyfill, 入口为main.ts
  resolve://解析
    {
      extensions: ['.ts', '.js', '.json']//解析这几种扩展类型
    },
};
