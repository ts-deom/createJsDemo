import * as createjs from 'createjs-module';
import state = require("stats.js");

window.onload = function(){
    var myCanvas = document.createElement("canvas");
    myCanvas.id = "myCanvas";
    document.body.appendChild(myCanvas);

    var stage: createjs.Stage = new createjs.Stage('myCanvas');

    var circle = new createjs.Shape();
    circle.graphics.beginFill("#ff0000").drawCircle(0,0,50);
    circle.x = 100;
    circle.y = 100;
    circle.addEventListener("click",function(e){
        circle.x ++;
    })

    stage.addChild(circle);
    stage.update();
    console.log(stage);
    console.log(circle);


    var fps = new state();
    console.log(fps);
    this.document.body.appendChild(fps.dom);

    requestAnimationFrame(animate);
    function animate(){
        stage.update();
        fps.update();
        requestAnimationFrame(animate);
    }

}



